package com.atlassian.devrel.confluence.extractor;

import com.atlassian.confluence.content.CustomContentEntityObject;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.BodyType;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.plugins.index.api.Extractor2;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.plugins.index.api.StringFieldDescriptor;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.v2.macro.MacroManager;
import com.atlassian.devrel.confluence.fieldhandler.ContainerMacroFieldHandler;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.function.Function;

import static com.atlassian.confluence.core.BodyType.WIKI;
import static com.atlassian.confluence.core.BodyType.XHTML;
import static com.google.common.base.Preconditions.checkNotNull;

public class ContainerMacroExtractor implements Extractor2 {

    private final static String SCROLL_PLUGIN_KEY = "com.k15t.scroll.scroll-platform:scroll-search-proxy-content-type";
    public final static String CONTAINER_MACROS_FIELD_NAME = "containerMacros";

    private final Logger log = LoggerFactory.getLogger(ContainerMacroFieldHandler.class);

    private XhtmlContent xhtmlContent;
    private MacroManager macroManager;

    public ContainerMacroExtractor(@ComponentImport XhtmlContent xhtmlContent, @ComponentImport MacroManager macroManager) {
        this.xhtmlContent = checkNotNull(xhtmlContent);
        this.macroManager = checkNotNull(macroManager);
    }

    @Override
    public StringBuilder extractText(Object searchable) {
        return new StringBuilder();
    }

    @Override
    public Collection<FieldDescriptor> extractFields(Object searchable) {
        ImmutableList.Builder<FieldDescriptor> resultBuilder = ImmutableList.builder();
        if (searchable instanceof CustomContentEntityObject) {
            CustomContentEntityObject customEntity = (CustomContentEntityObject) searchable;

            if (customEntity.getPluginModuleKey().equals(SCROLL_PLUGIN_KEY)) {
                ContentEntityObject container = customEntity.getContainer();

                MacroCollector collector = new MacroCollector(macroManager);
                BodyType bodyType = container.getBodyContent().getBodyType();
                if (bodyType.equals(XHTML)) {
                    processXhtml(container, collector);
                } else if (bodyType.equals(WIKI)) {
                    collector.processPotentialWikiMacro(container.getBodyAsString());
                }
                Function<String, FieldDescriptor> toContainerMacroFieldDescriptor = macroName -> new StringFieldDescriptor(CONTAINER_MACROS_FIELD_NAME, macroName, FieldDescriptor.Store.NO);
                collector.getMacroNames().stream().map(toContainerMacroFieldDescriptor).forEach(resultBuilder::add);
            }
        }
        return resultBuilder.build();
    }

    private void processXhtml(final ContentEntityObject searchableCeo, final MacroDefinitionHandler macroUsageCollector) {
        DefaultConversionContext context = new DefaultConversionContext(searchableCeo.toPageContext());
        try {
            xhtmlContent.handleMacroDefinitions(searchableCeo.getBodyAsString(), context, macroUsageCollector);
        } catch (XhtmlException ex) {
            log.warn("Failed to extracting macro usages on entity [{}] : {}", searchableCeo.getId(), ex.getMessage());
            log.debug("Failed to extracting macro usages on entity [{}] : {}", searchableCeo.getId(), ex);
        }
    }
}
