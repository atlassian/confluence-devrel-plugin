package it.webdriver.devrel.confluence;

import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.component.editor.EditorContent;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CreatePage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.github.javafaker.Faker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(ConfluenceStatelessTestRunner.class)
public class DevRelPageStatelessTest {

    @Inject
    private static ConfluenceTestedProduct product;

    @Fixture
    private static final UserFixture user = userFixture().build();
    @Fixture
    private static final UserFixture anotherUser = userFixture().build();
    @Fixture
    private static final SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .permission(anotherUser, REGULAR_PERMISSIONS)
            .build();

    private static final Faker faker = Faker.instance();

    @Test
    public void editingAndPublishing() {
        final CreatePage createPage = product.loginAndCreatePage(user.get(), space.get());
        final String title = format("%s - %s", faker.rickAndMorty().character(), faker.rickAndMorty().location());
        final String content = faker.rickAndMorty().quote();

        createPage.setTitle(title);
        EditorContent editorContent = createPage.getEditor().getContent();
        editorContent.type(content);
        editorContent.type(Keys.ENTER.toString());

        ViewPage publishedPage = createPage.save();
        assertThat(publishedPage.getTitle(), equalTo(title));
        waitUntil(publishedPage.getRenderedContent().getTextTimed(), containsString(content));
    }

}
