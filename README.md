# Confluence DevRel Plugin

This repo introduces the build configurations and development best practices from your
favourite Confluence Enterprise/Platform Team

## dev setup

* start confluence with the plugin installed in it

```bash
mvn confluence:run -pl plugin
```

* start confluence with debug port open

```bash
mvn confluence:debug -pl plugin
```

* refresh the instance with plugin updates using QuickReload

```bash
mvn package -DskipTests -pl plugin
```
