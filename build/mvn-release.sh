#! /usr/bin/env bash

function milestone() {
  timestamp=$(date '+%Y%m%d')
  hash=$(git rev-parse --short HEAD)
  branch_name="milestone/${timestamp}-${hash}"

  git checkout -b "${branch_name}"
  git push origin "${branch_name}"

  # shellcheck disable=SC2016
  project_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.6.0:exec)

  if [[ ${project_version} =~ ^([0-9]+)\.([0-9]+)\.([0-9]+)-SNAPSHOT$ ]];
  then
      major_version=${BASH_REMATCH[1]}
      minor_version=${BASH_REMATCH[2]}
      patch_version=${BASH_REMATCH[3]}
  else
      echo invalid snapshot version
      exit 1
  fi

  milestone_version="${major_version}.${minor_version}.${patch_version}-${timestamp}.${hash}"

  mvn -DskipTests -Darguments=-DskipTests \
    release:clean \
    release:prepare \
    release:perform \
    -DreleaseVersion="${milestone_version}" \
    -DdevelopmentVersion="${project_version}" \
    -DscmCommentPrefix="[skip ci][maven-release]"
}

function final() {
  mvn -DskipTests -Darguments=-DskipTests \
    release:clean \
    release:prepare \
    release:perform \
    -DscmCommentPrefix="[skip ci][maven-release]"
}

eval "$@"